#include <stdio.h>
#include <limits.h>
#include <math.h>

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

const char bot = 'X';
const char plr = 'O';
const char empty = ' ';
char b[3][4]={"X  ","   ","   "};

int W(int c) {return ((b[0][0]==c&&b[1][0]==c&&b[2][0]==c) || // Horizontal
					  (b[0][1]==c&&b[1][1]==c&&b[2][1]==c) ||
					  (b[0][2]==c&&b[1][2]==c&&b[2][2]==c) ||
					  (b[0][0]==c&&b[0][1]==c&&b[0][2]==c) || // Vertical
					  (b[1][0]==c&&b[1][1]==c&&b[1][2]==c) ||
					  (b[2][0]==c&&b[2][1]==c&&b[2][2]==c) ||
					  (b[0][0]==c&&b[1][1]==c&&b[2][2]==c) || // Diagonals
					  (b[2][0]==c&&b[1][1]==c&&b[0][2]==c));}

float
minimax(int depth, int isbot, float alpha, float beta)
{
	if (W(plr)) return -1;
	else if (W(bot)) return 1;
	int done = 0;
	float best = isbot ? -INFINITY : INFINITY;
	for (int y = 0; y < 3 && !done; y++) {
		for (int x = 0; x < 3 && !done; x++) {
			if (b[y][x] == empty) {
				b[y][x] = isbot ? bot : plr;
				float score = minimax(depth+1, !isbot, alpha, beta);
				b[y][x] = empty;
				if (isbot) {
					best = MAX(best, score);
					alpha = fmax(alpha, best);
				} else {
					best = MIN(best, score);
					beta = fmin(beta, best);
				}
				if (beta <= alpha) done = 1;
			}
		}
	}
	return best/(float)depth;
}

int
main(void)
{
	for (;;) {
		for (int i = 0; i < 3; i++) puts(b[i]);

		if (W(plr) == 1) return printf("You win!\n")-9;
		else if (W(bot) == 1) return printf("You lose!\n")-10;

		int x = 1, y = 1;
		for (;b[y-1][x-1]!=empty;) {
			x = getchar()-'0';getchar();
			y = getchar()-'0';getchar();
		}
		b[y-1][x-1] = plr;

		float best = -INFINITY;
		int bestx = 0, besty = 0;
		for(y = 0; y < 3; y++) {
			for(x = 0; x < 3; x++) {
				if(b[y][x] == empty){
					b[y][x] = bot;
					float score = minimax(1, 0, -INFINITY, INFINITY);
					b[y][x] = empty;
					if(score > best) {
						best = score;
						bestx = x;
						besty = y;
					}
				}
			}
		}
		b[besty][bestx] = bot;
	}
}
